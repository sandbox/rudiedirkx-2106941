(function($) {

  Drupal.behaviors.nodebloxAdmin = {
    attach: function(context, settings) {

      $('.nodeblox-view-name', context).bind('change', function(e) {
        var label = this.options[this.selectedIndex].text;
        var $display = $(this).closest('fieldset').find('.nodeblox-display-name');
        var $option = $display.find('optgroup[label="' + label + '"]');
        $option.children()[0].selected = true;
      });

    }
  };

})(jQuery);
