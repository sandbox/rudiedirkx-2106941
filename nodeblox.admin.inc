<?php

/**
 * Form callback for admin/content/nodeblox.
 */
function nodeblox_choose_form($form, &$form_state) {
  $nodeblox = _nodeblox_get();

  $sources = array();
  foreach ($nodeblox as $nb) {
    $source_id = $nb->view . ':' . $nb->display;
    if (empty($sources[$source_id])) {
      $sources[$source_id] = _nodeblox_options($nb);
    }
  }

  $form['#attributes']['class'][] = 'nodeblox-choose';
  $form['#attached']['css'][] = drupal_get_path('module', 'nodeblox') . '/nodeblox.admin.css';

  $form['nodeblox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Choose nodes'),
    '#tree' => TRUE,
  );
  foreach ($nodeblox as $delta => $nb) {
    $source_id = $nb->view . ':' . $nb->display;
    $moddelta = 'nodeblox/' . $delta;
    $form['nodeblox'][$delta] = array(
      '#type' => 'select',
      '#title' => $nb->admin_title,
      '#options' => $sources[$source_id],
      '#default_value' => $nb->nid,
      '#empty_option' => '- None -',
    );
    if (user_access('administer blocks')) {
      $form['nodeblox'][$delta]['#description'] = t('Block config: !link', array(
        '!link' => l($moddelta, "admin/structure/block/manage/$moddelta/configure")
      ));
    }
    if (isset($_GET['open']) && $_GET['open'] == $delta) {
      $form['nodeblox'][$delta]['#attributes']['class'][] = 'nodeblox-hilited';
      $form['nodeblox'][$delta]['#weight'] = -1;
    }
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#submit'][] = 'nodeblox_choose_form_submit';

  // Add dirty JS to add dirty CSS to hilite a block.
  if (isset($_GET['open'], $nodeblox[ $_GET['open'] ])) {
    $form['#attached']['js'][] = array(
      'type' => 'inline',
      'data' => "jQuery('select.nodeblox-hilited').closest('.form-item').addClass('nodeblox-hilited');",
      'scope' => 'footer',
    );
  }

  return $form;
}

/**
 * Submit callback for nodeblox_choose_form().
 */
function nodeblox_choose_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  $nodeblox_nids = $values['nodeblox'];
  variable_set('nodeblox_nids', $nodeblox_nids);

  drupal_set_message(t('Nodeblox selection saved.'));
}

/**
 * Form callback for admin/structure/nodeblox.
 */
function nodeblox_structure_form($form, &$form_state) {
  $nodeblox = _nodeblox_get();

  $form['#attributes']['class'][] = 'nodeblox-structure';
  $form['#attached']['css'][] = drupal_get_path('module', 'nodeblox') . '/nodeblox.admin.css';
  $form['#attached']['js'][] = drupal_get_path('module', 'nodeblox') . '/nodeblox.admin.js';

  $view_names = db_query('
    SELECT name, human_name
    FROM {views_view}
    WHERE tag = ?
    ORDER BY name
  ', array('nodeblox'))->fetchAllKeyed();

  $display_names = array();
  if ($view_names) {
    $query = db_query('
      SELECT v.name AS view, d.id, d.display_title
      FROM {views_display} d
      JOIN {views_view} v ON (d.vid = v.vid)
      WHERE v.name IN (:views)
      ORDER BY d.id
    ', array(':views' => array_keys($view_names)));
    foreach ($query as $display) {
      $display_names[ $view_names[$display->view] ][$display->id] = $display->display_title;
    }
  }

  $entity_info = entity_get_info('node');
  $view_modes = array('default' => 'Default');
  foreach ($entity_info['view modes'] as $name => $mode) {
    if (@$mode['custom settings']) {
      $view_modes[$name] = $mode['label'];
    }
  }

  $required = function($delta) use ($nodeblox) {
    return !$nodeblox || $delta;
  };
  $open = function($delta) {
    return @$_GET['open'] ? $delta === $_GET['open'] : !$delta;
  };

  // NEW
  $nodeblox['0'] = (object) array('admin_title' => '');

  // @todo Make pretty vertical tabs with summary.

  $form['blocks'] = array('#tree' => TRUE);
  foreach ($nodeblox as $delta => $info) {
    $form['blocks'][$delta] = array(
      '#info' => $info,

      '#type' => 'fieldset',
      '#title' => $delta ? $delta . ': ' . $info->admin_title : t('New block'),
      '#collapsed' => !$open($delta),
      '#collapsible' => !$open($delta),

      'admin_title' => array(
        '#type' => 'textfield',
        '#title' => t('Admin title'),
        '#default_value' => @$info->admin_title,
        '#required' => $required($delta),
      ),
      'delta' => array(
        '#type' => 'machine_name',
        '#title' => t('Machine name'),
        '#default_value' => $delta ?: '',
        '#disabled' => (bool) $delta,
        '#machine_name' => array(
          'source' => array('blocks', $delta, 'admin_title'),
          'exists' => '_nodeblox_delta_exists',
        ),
        '#required' => $required($delta),
      ),
      'view_mode' => array(
        '#type' => 'select',
        '#title' => t('View mode'),
        '#options' => $view_modes,
        '#default_value' => @$info->view_mode,
        '#required' => $required($delta),
      ),
      'view' => array(
        '#type' => 'select',
        '#title' => t('View name'),
        '#options' => $view_names,
        '#default_value' => @$info->view,
        '#required' => $required($delta),
        '#attributes' => array('class' => array('nodeblox-view-name')),
      ),
      'display' => array(
        '#type' => 'select',
        '#title' => t('View display name'),
        '#options' => $display_names,
        '#default_value' => @$info->display,
        '#required' => $required($delta),
        '#attributes' => array('class' => array('nodeblox-display-name')),
      ),
      'title' => array(
        '#type' => 'textfield',
        '#title' => t('Block title'),
        '#default_value' => @$info->title,
        '#description' => t('Accepts node tokens: <code>[node:title]</code> etc.'),
      ),
      'links' => array(
        '#type' => 'checkbox',
        '#title' => t('Show links'),
        '#default_value' => @$info->links,
      ),
    );
  }

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#tree' => TRUE,
  );
  $form['settings']['nodeblox_admin_title_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin title prefix'),
    '#default_value' => variable_get('nodeblox_admin_title_prefix'),
  );
  $form['settings']['nodeblox_show_empty_blocks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show empty blocks'),
    '#default_value' => variable_get('nodeblox_show_empty_blocks'),
    '#description' => t('Enables contextual links, creates a placeholder etc.'),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#submit'][] = 'nodeblox_structure_form_submit';

  return $form;
}

/**
 * Submit callback for nodeblox_structure_form().
 */
function nodeblox_structure_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  $save = array();
  foreach ($values['blocks'] as $block) {
    if (!@$block['delete']) {
      if ($block['admin_title'] && $block['delta'] && $block['view_mode'] && $block['view'] && $block['display']) {
        $delta = $block['delta'];
        unset($block['delete'], $block['delta']);

        $save[$delta] = (object) $block;
      }
    }
  }

  variable_set('nodeblox', $save);

  foreach ($values['settings'] as $name => $value) {
    variable_set($name, $value);
  }
}
